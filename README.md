This application is the rest api for image data manipulation

Pre-requisites
--------------
You need
-NodeJS
-MongoDB 
installed on your environment

Steps
-----
1. Clone the repo
2. navigate to the root directory
3. Run the command `node index.js`