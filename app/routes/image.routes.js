module.exports = (app) => {
    const images = require('../controllers/image.controller.js');

    // Retrieve a set of images by given count
    app.get('/images/:slideCount', images.getNoOfSlides);

    // define a simple route
    app.get('/', (req, res) => {
        res.json({"message": "Welcome to test image service. Organize and keep track of all your data quickly."});
    });
}