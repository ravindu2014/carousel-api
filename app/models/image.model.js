const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
    image: String,
    title: String,
    subtitle: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Image', ImageSchema);