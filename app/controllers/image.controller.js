const Image = require('../models/image.model.js');

// Create and Save set of Images. Change the header and implementation when this is being performed by api requests
exports.createAll = () => {
    var imageData = [
        {
            image: 'http://localhost:3600/images/image1.jpg',
            title: 'Light mask',
            subtitle: 'First text'
        },
        {
            image: 'http://localhost:3600/images/image2.jpg',
            title: 'Landscape',
            subtitle: 'Second text'
        },
        {
            image: 'http://localhost:3600/images/image3.jpg',
            title: 'Dessert',
            subtitle: 'Third text'
        },
        {
            image: 'http://localhost:3600/images/image4.jpg',
            title: 'River',
            subtitle: 'Fourth text'
        },
        {
            image: 'http://localhost:3600/images/image5.jpg',
            title: 'Lake',
            subtitle: 'Fifth text'
        },
        {
            image: 'http://localhost:3600/images/image6.jpg',
            title: 'Waterfall',
            subtitle: 'Sixth text'
        },
        {
            image: 'http://localhost:3600/images/image7.jpg',
            title: 'Sand',
            subtitle: 'Seventh text'
        },
        {
            image: 'http://localhost:3600/images/image8.jpg',
            title: 'Bridge',
            subtitle: 'Eighth text'
        },
        {
            image: 'http://localhost:3600/images/image9.jpg',
            title: 'Mountain',
            subtitle: 'Ninth text'
        },
        {
            image: 'http://localhost:3600/images/image10.jpg',
            title: 'Building',
            subtitle: 'Tenth text'
        }
    ];

    Image.deleteMany({}, (err, data) => {
        if (err != null) {
            return console.log(err);
        } else {
            console.log('Deleted all the existing data from DB');
        }        
    });

    Image.insertMany(imageData, forceServerObjectId = true, (err, data) => {
        if (err != null) {
            return console.log(err);
        }
        console.log('Created new images data successfully in DB');
    });
};

// find number of images as given in the count parameter
exports.getNoOfSlides = (req, res) => {
    var slideCount = req.params.slideCount;

    if (!slideCount || isNaN(slideCount) || slideCount > 10 || slideCount < 1) {
        return res.status(400).json({data: [], message: "Please parse a valid image count in between 1-10"});
    }

    Image.find().sort({_id:1}).limit(slideCount)
    .then(images => {
        res.status(200).json({data: images, message: "Images successfully received"});
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving Images."
        });
    });
};
