const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");
const port = 3600; 

const app = express();

// Serves resources from public folder
var path = require('path');
app.use(express.static(path.join(__dirname, 'public')));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// Avoid cross origin restriction when running in the local
app.use(cors());

require('./app/routes/image.routes.js')(app);

// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// Insert set of Image data initially. Remove below 2 lines when the create functionality is implemented
const imagesController = require('./app/controllers/image.controller.js');
imagesController.createAll();

// listen for requests
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});